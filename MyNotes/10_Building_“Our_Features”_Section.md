# 10: Building “Our Features” Section

## 28: Styling Our Features (Part 1)

Done. Lots of changes, but not much to post.

---

## 29: Styling Our Features (Part 2)

Done

---

## Current Status of Files

### index.html

```html
  <!DOCTYPE html>
  <html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <title>Clear View Escapes</title>
    <meta name="keywords" content="Travel planning, travel bundles, travel escapes, affordable travel">
    <meta name="description" content="Your clarity. One trip away. We create soul restoring journeys that inspire you to be you.">
    <link rel="stylesheet" href="temp/styles/style.css">
  </head>
  <body>
    <header>
    <img src="assets/images/icons/clear-view-escapes.svg">
    <a href="#" class="btn">Get in Touch</a>
    <nav>
      <ul>
      <li><a href="#our-beginning">Our Beginning</a></li>
      <li><a href="#features">Features</a></li>
      <li><a href="#testimonials">Testimonials</a></li>
      </ul>
    </nav>
    </header>
    <div class="large-hero">
    <picture>
      <source srcset="assets/images/hero--large.jpg 1920w, assets/images/hero--large-hi-dpi.jpg 3840w" media="(min-width: 1380px)">
      <source srcset="assets/images/hero--medium.jpg 1380w, assets/images/hero--medium-hi-dpi.jpg 2760w" media="(min-width: 990px)">
      <source srcset="assets/images/hero--small.jpg 990w, assets/images/hero--small-hi-dpi.jpg 1980w" media="(min-width: 640px)">
      <img srcset="assets/images/hero--smaller.jpg 640w, assets/images/hero--smaller-hi-dpi.jpg 1280w" alt="Coastal view of ocean and mountains" class="large-hero__image">
    </picture>
    <div class="large-hero__text-content">
      <div class="wrapper">
      <h1 class="large-hero__title">Your clarity</h1>
      <h2 class="large-hero__subtitle">One trip away.</h2>
      <p class="large-hero__description">We create soul restoring journeys that inspire you to be you.</p>
      <p><a href="#" class="btn btn--orange btn--large">Get Started Today</a></p>
      </div>
    </div>
    </div>
    <div id="our-beginning" class="page-section">
    <div class="wrapper">
      <h2 class="headline headline--centered headline--light headline--b-margin-small">The first trip we planned <strong>was our own.</strong></h2>
      <h3 class="headline headline--centered headline--orange headline--small headline--narrow headline--light headline--b-margin-large">Ever since, we&rsquo;ve been working to make travel <strong>better for everyone.</strong></h3>

      <div class="wrapper wrapper--medium wrapper--b-margin">
      <img sizes="(min-width: 970px) 976px, 100vw" srcset="assets/images/first-trip-low-res.jpg 565w, assets/images/first-trip.jpg 976w, assets/images/first-trip-hi-dpi.jpg 1952w" alt="Couple walking down a street.">
      </div>
      <div class="row row--gutters">
      <div class="row__medium-4 row__medium-4--larger row__b-margin-until-medium">
        <picture>
        <source sizes="404px" srcset="assets/images/our-start.jpg 404w, assets/images/our-start-hi-dpi.jpg 808w" media="(min-width: 1020px)">
        <source sizes="320px" srcset="assets/images/our-start-portrait.jpg 382w, assets/images/our-start-portrait-hi-dpi.jpg 764w" media="(min-width: 800px)">
        <img srcset="assets/images/our-start-landscape.jpg 800w, assets/images/our-start-landscape-hi-dpi.jpg 1600w" alt="our founder, jane doe">
        </picture>  
      </div>
      <div class="row__medium-8 row__medium-8--smaller">
        <div class="generic-content-container">
        <h2 class="headline headline--no-t-margin">Here&rsquo;s how we got started&hellip;</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#">quis nostrud exercitation</a> ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <p>Duis aute irure dolor in <strong>reprehenderit in</strong> voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum velit esse cillum <strong>dolore eu fugiat.</strong></p>
        </div>
      </div>
      </div>
    </div>
    </div>
    <div id="features" class="page-section page-section--blue">
    <div class="wrapper">  
      <h2 class="section-title"><img class="section-title__icon" src="assets/images/icons/star.svg">Our <strong>Features</strong></h2>

      <div class="row row--gutters-large generic-content-container">
      <div class="row__medium-6">
        <div class="feature-item">
        <img class="feature-item__icon" src="assets/images/icons/rain.svg">
        <h3 class="feature-item__title">We&rsquo;ll Watch the Weather</h3>
        <p>Download our app and we&rsquo;ll send you a notice if it&rsquo;s about to rain in the next 20 minutes around your current location. A good rain can be refreshing, but sometimes you just want to stay dry.</p>
        </div>
        <div class="feature-item">
        <img class="feature-item__icon" src="assets/images/icons/globe.svg">
        <h3 class="feature-item__title">Global Guides</h3>
        <p>We&rsquo;ve scoured the entire planet for the best retreats and beautiful vistas. If there&rsquo;s a corner of the world you want to escape to we know the most scenic and inspiring locations.</p>
        </div>
      </div>
      <div class="row__medium-6">
        <div class="feature-item">
        <img class="feature-item__icon" src="assets/images/icons/wifi.svg">
        <h3 class="feature-item__title">Wi-Fi Waypoints</h3>
        <p>We only send you on trips to places we can personally vouch for as being amazing. Which means we&rsquo;ve mapped out where local wi-fi spots are and marked them in our app&rsquo;s map view.</p>
        </div>

        <div class="feature-item">
        <img class="feature-item__icon" src="assets/images/icons/fire.svg">
        <h3 class="feature-item__title">Survival Kit</h3>
        <p>Everytime you book an escape with us we send you a survival kit with the finest materials. The kit will allow you to setup a tent, start a fire, scratch your own back and lower your taxes.</p>
        </div>
      </div>
      </div>
      </div>
    </div>
    <div id="testimonials">
    <img src="assets/images/icons/comment.svg">
    <h2>Real Testimonials</h2>
    <img src="assets/images/testimonial-jane.jpg">
    <h3>Jane Doe</h3>
    <h3>9 Time Escaper</h3>
    <p>&ldquo;Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&rdquo;</p>
    <img src="assets/images/testimonial-john.jpg">
    <h3>John Smith</h3>
    <h3>4 Time Escaper</h3>
    <p>&ldquo;Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur.&rdquo;</p>
    <img src="assets/images/testimonial-cat.jpg">
    <h3>Cat McKitty</h3>
    <h3>6 Time Escaper</h3>
    <p>&ldquo;Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.&rdquo;</p>
    </div>
    <footer>
    <p>Copyright &copy; 2016 Clear View Escapes. All rights reserved. <a href="#" class="btn btn--orange">Get in Touch</a></p>
    </footer>
  </body>
  </html>
```

## apps/assets/styles

### style.css

```css
  @import "normalize.css";
  @import "base/_variables.css";
  @import "base/_mixins.css";
  @import "base/_global.css";
  @import "modules/_large-hero.css";
  @import "modules/_btn.css";
  @import "modules/_wrapper.css";
  @import "modules/_page-section.css";
  @import "modules/_headline.css";
  @import "modules/_row.css";
  @import "modules/_generic-content-container.css";
  @import "modules/_section-title.css";
  @import "modules/_feature-item.css";
```

### /base

### _global.css

```css
  body {
    font-family: 'Roboto', sans-serif;
    color: #333;
  }
  img {
    max-width: 100%;
    height: auto;
  }
  * {
    box-sizing: border-box;
  }
  a {
    color: $mainOrange;
  }
```

### _mixins.css

```css
  @define-mixin atSmall {
    @media (min-width: 530px) {
      @mixin-content;
    }
  }
  @define-mixin atMedium {
    @media (min-width: 800px) {
      @mixin-content;
    }
  }
  @define-mixin atLarge {
    @media (min-width: 1200px) {
      @mixin-content;
    }
  }
  @define-mixin clearfix {
    &::after {
      content: "";
      clear: both;
      display: table;
    }
  }
```

### _variables.css

```css
  $mainBlue: #2f5572;
  $mainOrange: #F18407;
```

### /modules

### _btn.css

```css
  .btn {
    background-color: $mainBlue;
    color: #FFF;
    text-decoration: none;
    padding: .75rem 1.2rem;
    display: inline-block;
    &--orange {
      background-color: #d59541;
    }
    &--large {
      padding: 1.1rem 1.9 rem;
      @mixin atSmall {
        font-size: 1.25rem;
      }
    }
  }
```

### _feature-item.css

```css
.feature-item {
  position: relative;
  padding-bottom: 2.5rem;
  
  @mixin atSmall {
    padding-left: 94px;
  }
  
  &__icon {
    display: block;
    margin: 0 auto;
  
    @mixin atSmall {
    position: absolute;
    left: 0;
    }
  }
  
  &__title {
    font-size: 1.6rem;
    font-weight: 300;
    margin-top: .7em;
    margin-bottom: .8rem;
    text-align: center;
  
    @mixin atSmall {
    text-align: left;
    margin-top: 1em;
    font-size: 1.875rem;
    }
  }
  }
```

### _generic-content-container.css

```css
  .generic-content-container {
  p {
    font-weight: 300;
    line-height: 1.65;
    margin: 0 0 1.8rem 0;
    @mixin atSmall {
      font-size: 1.125rem;
    }
  }
  p a {
    font-weight: 700;
  }
  }
```

### _headline.css

```css
  .headline {
    font-weight: 300;
    font-size: 1.9rem;
    color: $mainBlue;
    @mixin atSmall {
      font-size: 2.875rem;
    }
    &--centered {
      text-align: center;
    }
    &--orange {
      color: $mainOrange;
    }
    &--small {
      font-size: 1.2rem;
      @mixin atSmall {
        font-size: 1.875rem;
      }
    }
    &--narrow {
      max-width: 500px;
      margin-left: auto;
      margin-right: auto;
    }
    &--light {
      font-weight: 100;
    }
    &--b-margin-small {
      margin-bottom: 0.5em;
    }
    &--b-margin-large {
      margin-bottom: 1.6em;
    }
    &--no-t-margin {
      margin-top: 0;
    }
  }
```

### _large-hero.css

```css
.large-hero {
  border-bottom: 10px solid $mainBlue;
  position: relative;
  
  &__image {
    display: block;
  }
  &__text-content {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: 0;
    width: 100%;
    text-align: center;
  }
  
  &__title {
    font-weight: 300;
    color: $mainBlue;
    margin: 0;
    font-size: 2.4rem;

    @mixin atSmall {
    font-size: 4.8rem;
    }
  
  }
  
  &__subtitle {
    font-weight: 300;
    color: $mainBlue;
    font-size: 1.5rem;
    margin: 0;
  
    @mixin atSmall {
    font-size: 2.9rem;
    }
  
  }
  
  &__description {
    color: #FFF;
    font-size: 1.1rem;
    font-weight: 100;
    text-shadow: 2px 2px 0 rgba(0, 0, 0, .1);
    max-width: 30rem;
    margin-left: auto;
    margin-right: auto;
    @mixin atSmall {
    font-size: 1.875rem;
    }
  }
  
  }
```

### _page-section.css

```css
  .page-section {
    padding: 1.2rem 0;
    @mixin atMedium {
      padding: 4.5rem 0;
    }
    &--blue {
      background-color: $mainBlue;
      color: #FFF;
    }
  }
```

### _row.css

```css
  .row {
  @mixin clearfix;
  &--gutters {
    margin-right: -65px;
  }
  &--gutters-large {
    margin-right: -100px;
  }
  &--gutters > div {
    padding-right: 65px;
  }
  &--gutters-large > div {
    padding-right: 100px;
  }
  &__b-margin-until-medium {
    margin-bottom: 1rem;
  }
  @mixin atMedium {
    &__b-margin-until-medium {
      margin-bottom: 0;
    }
    &__medium-4 {
      float: left;
      width: 33.33%;
    }
    &__medium-4--larger {
      width: 37%;
    }
    &__medium-6 {
      float: left;
      width: 50%;
    }

    &__medium-8 {
      float: left;
      width: 66.66%;
    }
    &__medium-8--smaller {
      width: 63%;
    }
  }
  }
```

### _section-title.css

```css
.section-title {
  font-size: 2.55rem;
  font-weight: 300;
  text-align: center;
  
  @mixin atSmall {
    font-size: 3.75rem;
  }
  
  strong {
    font-weight: 500;
  }
  
  &__icon {
    display: block;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: .6rem;
  
    @mixin atSmall {
    margin-right: .5rem;
    margin-left: 0;
    margin-bottom: 0;
    display: inline-block;
    }
  }
  }
```
