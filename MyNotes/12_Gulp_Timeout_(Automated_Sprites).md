# 12: Gulp Timeout (Automated Sprites)

## 33: Automatic Sprites with Gulp (Part 1)

Installed gulp-svg-sprite, which creates a sprite file, a combined image file that saves bandwidth

### gulpfile.js

```js
  require('./gulp/tasks/styles'),
  require('./gulp/tasks/watch'),
  require('./gulp/tasks/sprites');
```

### sprites.js

```js
  var gulp = require('gulp');
  svgSprite = require('gulp-svg-sprite');
  var config = {
    mode: {
      css: {
        render: {
          css: {
            template: './gulp/templates/sprite.css'
          }
        }
      }
    }
  }
  gulp.task('createSprite', function() {
    return gulp.src('./app/assets/images/icons/**/*.svg')
      .pipe(svgSprite(config))
      .pipe(gulp.dest('./app/temp/sprite/'));
  });
```

### sprite.css

```js
  {{#shapes}}
    .icon--{{base}} {
      width: {{width.outer}}px;
      height: {{height.outer}}px;
      background-image: url('/temp/sprite/css/{{{sprite}}}');
      background-position: {{position.relative.xy}};
    }
  {{/shapes}}
```

### temp/sprite/css/sprite.css - created by running `**gulp createSprite**`

```css
    .icon--clear-view-escapes {
      width: 142.4px;
      height: 59.53px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 0 0;
    }
    .icon--comment {
      width: 64px;
      height: 64px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 99.70443349753694% 0;
    }
    .icon--facebook {
      width: 21.23px;
      height: 42.01px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 26.040607071652357% 45.795830448496034%;
    }
    .icon--fire {
      width: 56px;
      height: 64px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 0 99.56481481481481%;
    }
    .icon--globe {
      width: 64px;
      height: 64px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 27.586206896551722% 99.56481481481481%;
    }
    .icon--instagram {
      width: 42.01px;
      height: 42.01px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 63.2917018534157% 49.234556504346486%;
    }
    .icon--mail {
      width: 64px;
      height: 48px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 0 48.00806451612903%;
    }
    .icon--rain {
      width: 64px;
      height: 64px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 99.70443349753694% 59.25925925925926%;
    }
    .icon--star {
      width: 64px;
      height: 64px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 59.11330049261084% 99.56481481481481%;
    }
    .icon--twitter {
      width: 51.78px;
      height: 42.06px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 94.04330452560171% 98.50700323226104%;
    }
    .icon--wifi {
      width: 60px;
      height: 64px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 68.79227053140097% 0;
    }
    .icon--youtube {
      width: 34.75px;
      height: 42px;
      background-image: url('/temp/sprite/css/svg/sprite.css-d675eadb.svg');
      background-position: 36.69752421959096% 45.792307692307695%;
    }
```

---

## 34: Automatic Sprites with Gulp (Part 2)

### sprites.js

```js
  var gulp = require('gulp'),
  svgSprite = require('gulp-svg-sprite'),
  rename = require('gulp-rename');
  var config = {
    mode: {
      css: {
        sprite: 'sprite.svg',
        render: {
          css: {
            template: './gulp/templates/sprite.css'
          }
        }
      }
    }
  }

  gulp.task('createSprite', function() {
    return gulp.src('./app/assets/images/icons/**/*.svg')
      .pipe(svgSprite(config))
      .pipe(gulp.dest('./app/temp/sprite/'));
  });

  gulp.task('copySpriteGraphic', ['createSprite'], function() {
    return gulp.src('./app/temp/sprite/css/**/*.svg')
      .pipe(gulp.dest('./app/assets/images/sprites'));
  });

  gulp.task('copySpriteCSS', ['createSprite'], function() {
    return gulp.src('./app/temp/sprite/css/*.css')
      .pipe(rename('_sprite.css'))
      .pipe(gulp.dest('./app/assets/styles/modules'));
  });

  gulp.task('icons', ['createSprite', 'copySpriteGraphic', 'copySpriteCSS']);
```

### sprite.css

```css
  /* Do not edit modules/_sprite directly, as it is generated automatically by Gulp
  Instead edit gulp/templates/sprite */

  {{#shapes}}
    {{#first}}
    .icon {
      background-image: url('/assets/images/sprites/{{{sprite}}}');
    }
    {{/first}}
    .icon--{{base}} {
      width: {{width.outer}}px;
      height: {{height.outer}}px;
      background-position: {{position.relative.xy}};
    }
  {{/shapes}}
```

---

## 35: Automatic Sprites with Gulp (Part 3)

### sprites.js

```js
  var gulp = require('gulp'),
  svgSprite = require('gulp-svg-sprite'),
  rename = require('gulp-rename'),
  del = require('del');

  var config = {
    mode: {
      css: {
        sprite: 'sprite.svg',
        render: {
          css: {
            template: './gulp/templates/sprite.css'
          }
        }
      }
    }
  }

  gulp.task('beginClean', function() {
    return del(['./app/temp/sprite', './app/assets/images/sprites']);
  });

  gulp.task('createSprite', ['beginClean'], function() {
    return gulp.src('./app/assets/images/icons/**/*.svg')
      .pipe(svgSprite(config))
      .pipe(gulp.dest('./app/temp/sprite/'));
  });

  gulp.task('copySpriteGraphic', ['createSprite'], function() {
    return gulp.src('./app/temp/sprite/css/**/*.svg')
      .pipe(gulp.dest('./app/assets/images/sprites'));
  });

  gulp.task('copySpriteCSS', ['createSprite'], function() {
    return gulp.src('./app/temp/sprite/css/*.css')
      .pipe(rename('_sprite.css'))
      .pipe(gulp.dest('./app/assets/styles/modules'));
  });

  gulp.task('endClean', ['copySpriteGraphic', 'copySpriteCSS'], function () {
    return del('./app/temp/sprite');
  });

  gulp.task('icons', ['beginClean', 'createSprite', 'copySpriteGraphic', 'copySpriteCSS', 'endClean']);
```

---
