# 05: CSS Architecture

## 13: CSS File Architecture

1. File Architecture - Organize CSS into multiple, small files that each have a specific purpose
2. Identify Patterns in Design
3. Rules to Follow for Creating Class Names and Writing Our CSS Selectors

### index.html

```html
  <!DOCTYPE html>
  <html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <title>Clear View Escapes</title>
    <meta name="keywords" content="Travel planning, travel bundles, travel escapes, affordable travel">
    <meta name="description" content="Your clarity. One trip away. We create soul restoring journeys that inspire you to be you.">
    <link rel="stylesheet" href="temp/styles/style.css">
  </head>
  <body>
    <header>
    <img src="assets/images/icons/clear-view-escapes.svg">
    <a href="#">Get in Touch</a>
    <nav>
      <ul>
      <li><a href="#our-beginning">Our Beginning</a></li>
      <li><a href="#features">Features</a></li>
      <li><a href="#testimonials">Testimonials</a></li>
      </ul>
    </nav>
    </header>
    <div class="large-hero">
    <img src="assets/images/hero--large.jpg">
    <div class="large-hero__text-content">
      <h1>Your clarity.</h1>
      <h2>One trip away.</h2>
      <p>We create soul restoring journeys that inspire you to be you.</p>
      <p><a href="#">Get Started Today</a></p>
    </div>
    </div>
    <div id="our-beginning">
    <h2>The first trip we planned was our own.</h2>
    <h3>Ever since, we&rsquo;ve been working to make travel better for everyone.</h3>
    <img src="assets/images/first-trip.jpg" alt="Couple walking down a street.">
    <img src="assets/images/our-start.jpg" alt="Our founder, Jane Doe">
    <h2>Here&rsquo;s how we got started&hellip;</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#">quis nostrud exercitation</a> ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <p>Duis aute irure dolor in <strong>reprehenderit in</strong> voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum velit esse cillum <strong>dolore eu fugiat.</strong></p>
    </div>
    <div id="features">
    <img src="assets/images/icons/star.svg">
    <h2>Our Features</h2>

    <img src="assets/images/icons/rain.svg">
    <h3>We&rsquo;ll Watch the Weather</h3>
    <p>Download our app and we&rsquo;ll send you a notice if it&rsquo;s about to rain in the next 20 minutes around your current location. A good rain can be refreshing, but sometimes you just want to stay dry.</p>
    <img src="assets/images/icons/globe.svg">
    <h3>Global Guides</h3>
    <p>We&rsquo;ve scoured the entire planet for the best retreats and beautiful vistas. If there&rsquo;s a corner of the world you want to escape to we know the most scenic and inspiring locations.</p>
    <img src="assets/images/icons/wifi.svg">
    <h3>Wi-Fi Waypoints</h3>
    <p>We only send you on trips to places we can personally vouch for as being amazing. Which means we&rsquo;ve mapped out where local wi-fi spots are and marked them in our app&rsquo;s map view.</p>
    <img src="assets/images/icons/fire.svg">
    <h3>Survival Kit</h3>
    <p>Everytime you book an escape with us we send you a survival kit with the finest materials. The kit will allow you to setup a tent, start a fire, scratch your own back and lower your taxes.</p>
    </div>
    <div id="testimonials">
    <img src="assets/images/icons/comment.svg">
    <h2>Real Testimonials</h2>
    <img src="assets/images/testimonial-jane.jpg">
    <h3>Jane Doe</h3>
    <h3>9 Time Escaper</h3>
    <p>&ldquo;Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&rdquo;</p>
    <img src="assets/images/testimonial-john.jpg">
    <h3>John Smith</h3>
    <h3>4 Time Escaper</h3>
    <p>&ldquo;Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur.&rdquo;</p>
    <img src="assets/images/testimonial-cat.jpg">
    <h3>Cat McKitty</h3>
    <h3>6 Time Escaper</h3>
    <p>&ldquo;Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.&rdquo;</p>
    </div>
    <footer>
    <p>Copyright &copy; 2016 Clear View Escapes. All rights reserved. <a href="#">Get in Touch</a></p>
    </footer>
  </body>
  </html>
```

### style.css

```css
  @import "normalize.css";
  @import "base/_global.css";
  @import "modules/_large-hero.css";
```

### global.css

```css
  body {
    font-family: 'Roboto', sans-serif;
    color: #333;
  }
  img {
    max-width: 100%;
    height: auto;
  }
```

### _large-hero.css

```css
  .large-hero {
    position: relative;
  }
  .large-hero__text-content {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: 0;
    width: 100%;
    text-align: center;
  }
```

---

## 14: What is BEM?

* B = Block: A reusable, independent part of our design.

* E = Element: It belongs to a block and can not be used outside of it.

* M = Modifier: Used on a block or element to indicate a change from the default state for that block or element

* Lastly, BEM makes the relationship between our HTML and CSS clear.
  * There is no reason to code a pattern more than once!

### Changes to index.html

```html
    <div class="large-hero">
    <img src="assets/images/hero--large.jpg">
    <div class="large-hero__text-content">
      <h1 class="large-hero__title">Your clarity.</h1>
      <h2 class="large-hero__subtitle">One trip away.</h2>
      <p>We create soul restoring journeys that inspire you to be you.</p>
      <p><a href="#">Get Started Today</a></p>
    </div>
    </div>
```

### _large-hero.css

```css
  .large-hero {
    position: relative;
    &__text-content {
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      left: 0;
      width: 100%;
      text-align: center;
    }
    &__title {  // postcss will convert this to .large-hero__title
      font-weight: 300;
      color: #2f5572;
      font-size: 4.8rem;
    }

    &__subtitle {
      font-weight: 300;
      color: #2f5572;
      font-size: 2.9rem;
    }
  }
```

---

## 15: Complete Two Blocks

### index.html

```html
  <!DOCTYPE html>
  <html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <title>Clear View Escapes</title>
    <meta name="keywords" content="Travel planning, travel bundles, travel escapes, affordable travel">
    <meta name="description" content="Your clarity. One trip away. We create soul restoring journeys that inspire you to be you.">
    <link rel="stylesheet" href="temp/styles/style.css">
  </head>
  <body>
    <header>
    <img src="assets/images/icons/clear-view-escapes.svg">
    <a href="#" class="btn">Get in Touch</a>
    <nav>
      <ul>
      <li><a href="#our-beginning">Our Beginning</a></li>
      <li><a href="#features">Features</a></li>
      <li><a href="#testimonials">Testimonials</a></li>
      </ul>
    </nav>
    </header>
    <div class="large-hero">
    <img src="assets/images/hero--large.jpg">
    <div class="large-hero__text-content">
      <h1 class="large-hero__title">Your clarity.</h1>
      <h2 class="large-hero__subtitle">One trip away.</h2>
      <p class="large-hero__description">We create soul restoring journeys that inspire you to be you.</p>
      <p><a href="#" class="btn btn--orange btn--large">Get Started Today</a></p>
    </div>
    </div>
    <div id="our-beginning">
    <h2>The first trip we planned was our own.</h2>
    <h3>Ever since, we&rsquo;ve been working to make travel better for everyone.</h3>
    <img src="assets/images/first-trip.jpg" alt="Couple walking down a street.">
    <img src="assets/images/our-start.jpg" alt="Our founder, Jane Doe">
    <h2>Here&rsquo;s how we got started&hellip;</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#">quis nostrud exercitation</a> ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <p>Duis aute irure dolor in <strong>reprehenderit in</strong> voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum velit esse cillum <strong>dolore eu fugiat.</strong></p>
    </div>
    <div id="features">
    <img src="assets/images/icons/star.svg">
    <h2>Our Features</h2>

    <img src="assets/images/icons/rain.svg">
    <h3>We&rsquo;ll Watch the Weather</h3>
    <p>Download our app and we&rsquo;ll send you a notice if it&rsquo;s about to rain in the next 20 minutes around your current location. A good rain can be refreshing, but sometimes you just want to stay dry.</p>
    <img src="assets/images/icons/globe.svg">
    <h3>Global Guides</h3>
    <p>We&rsquo;ve scoured the entire planet for the best retreats and beautiful vistas. If there&rsquo;s a corner of the world you want to escape to we know the most scenic and inspiring locations.</p>
    <img src="assets/images/icons/wifi.svg">
    <h3>Wi-Fi Waypoints</h3>
    <p>We only send you on trips to places we can personally vouch for as being amazing. Which means we&rsquo;ve mapped out where local wi-fi spots are and marked them in our app&rsquo;s map view.</p>
    <img src="assets/images/icons/fire.svg">
    <h3>Survival Kit</h3>
    <p>Everytime you book an escape with us we send you a survival kit with the finest materials. The kit will allow you to setup a tent, start a fire, scratch your own back and lower your taxes.</p>
    </div>
    <div id="testimonials">
    <img src="assets/images/icons/comment.svg">
    <h2>Real Testimonials</h2>
    <img src="assets/images/testimonial-jane.jpg">
    <h3>Jane Doe</h3>
    <h3>9 Time Escaper</h3>
    <p>&ldquo;Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&rdquo;</p>
    <img src="assets/images/testimonial-john.jpg">
    <h3>John Smith</h3>
    <h3>4 Time Escaper</h3>
    <p>&ldquo;Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur.&rdquo;</p>
    <img src="assets/images/testimonial-cat.jpg">
    <h3>Cat McKitty</h3>
    <h3>6 Time Escaper</h3>
    <p>&ldquo;Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.&rdquo;</p>
    </div>
    <footer>
    <p>Copyright &copy; 2016 Clear View Escapes. All rights reserved. <a href="#" class="btn btn--orange">Get in Touch</a></p>
    </footer>
  </body>
  </html>
```

### variables.css

```css
  $mainBlue: #2f5572;
  $mainOrange: #2f5572;
```

### style.css

```css
  @import "normalize.css";
  @import "base/_global.css";
  @import "base/_variables.css";
  @import "modules/_large-hero.css";
  @import "modules/_btn.css";
```

### large-hero.css

```css
  .large-hero {
    position: relative;
    &__text-content {
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      left: 0;
      width: 100%;
      text-align: center;
    }
    &__title {
      font-weight: 300;
      color: $mainBlue;
      font-size: 4.8rem;
      margin: 0;
    }

    &__subtitle {
      font-weight: 300;
      color: $mainBlue;
      font-size: 2.9rem;
      margin: 0;
    }
    &__description {
      color: #FFF;
      font-size: 1.875rem;
      font-weight: 100;
      text-shadow: 2px 2px 0 rgba(0,0,0,.1);
      max-width: 30rem;
      margin-left: auto;
      margin-right: auto;
    }
  }
```

### btn.css

```css
  .btn {
    background-color: $mainBlue;
    color: #FFF;
    text-decoration: none;
    padding: .75rem 1.2rem;
    display: inline-block;
    &--orange {
      background-color: #d59541;
    }
    &--large {
      font-size: 1.25rem;
      padding: 1.1rem 1.9 rem;
    }
  }
```

---
