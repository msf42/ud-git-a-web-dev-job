# 09: GIT Timeout

## 27: GIT Branches

```bash
470  git branch # what branches we have

471  git status # which files have changed, etc

472  git add app/index.html # stages this file
473  git status
474  git add -A # stages all files
475  git status
476  git commit -m'Completed large hero and intro content blocks' #commit wcomment
477  git status
478  git branch count-to-ten # makes new branch, count-to-ten
479  git branch
480  git checkout count-to-ten # swichtes to that branch
481  git add -A
482  git status
483  git commit -m'counted to 4'
484  git checkout master # switch back to master
485  git checkout count-to-ten
486  git add -A
487  git commit -m'Counted to 7'
488  git add -A
489  git commit -m'Completed count to 10'
490  git checkout master
491  git merge count-to-ten # merge the branch to master
492  git push origin master # now push the changes
493  git branch count-to-fifteen
494  git checkout count-to-fifteen
495  git commit -am 'counted to 13'
496  git push origin count-to-fifteen
497  git commit -am 'completed count to 15'
498  git push origin count-to-fifteen
499  git checkout master
500  git pull origin master
501  git branch
502  git branch -d count-to-fifteen # delete a branch
503  git branch -d count-to-ten
504  git branch
505  git commit -am 'Removed test numbers'
506  git checkout -b our-features # new branch for next lesson
```

---
