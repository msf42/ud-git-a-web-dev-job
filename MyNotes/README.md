# GIT a Web Developer Job - Mastering the Modern Workflow

- [Sections 01 and 02: Overview & GIT Essentials](01_02_Overview_&_GIT_Essentials.md)

- [Section 03: Intro to Node.js and NPM](03_Intro_to_Node_js_and_NPM.md)

- [Section 04: Gulp Essentials](05_CSS_Architecture.md)

- [Section 05: CSS Architecture](05_CSS_Architecture.md)

- [Section 06: Gulp Timeout](06_Gulp_Timeout.md)

- [Section 07: Mobile-First Essentials](07_Mobile-First_Essentials.md)

- [Section 08: Let's Build!](08_Let’s_Build!.md)

- [Section 09: GIT Timeout](09_GIT_Timeout.md)

- [Section 10: Building the "Our Features" Section](10_Building_“Our_Features”_Section.md)

- [Section 11: Building "Testimonial" Section](11_Building_“Testimonial”_Section.md)

- [Section 12: Gulp Timeout (Automated Sprites)](12_Gulp_Timeout_(Automated_Sprites).md)

- [Section 13: Let's Finish Styling Our Site](13_Let’s_Finish_Styling_Our_Site.md)

- [Section 14: JavaScript Organization](14_JavaScript_Organization.md)

- [Section 15: Writing JavaScript for Our Header Menu](15_Writing_JavaScript_for_Our_Header_Menu.md)

---
