# 15: Writing JavaScript for Our Header Menu

## 43: How to Avoid jQuery Spaghetti

Set up the functionality for the mobile menu - the JS, clicking to reveal, etc. Next lesson will style it.

---

## 44: Adjusting Our Mobile Menu

Adjusted background, font size, etc.

---

## 45: Animating Hamburger Menu Icon Into an ‘X’

Lots of CSS.

---
