# 07: Mobile-First Essentials

## 19: What Does Mobile-First Mean?

### large-hero.css

```css
.large-hero {
  position: relative;
  &__text-content {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: 0;
    width: 100%;
    text-align: center;
  }
  &__title {
    font-weight: 300;
    color: $mainBlue;
    margin: 0;
    font-size: 1.1rem;
    @mixin atSmall {
      font-size: 2rem;        }
    }
    @mixin atMedium {
      font-size: 3.2rem;
    }
    @mixin atLarge {
      font-size: 4.8rem;
    }
  }

  &__subtitle {
    font-weight: 300;
    color: $mainBlue;
    font-size: 1.1rem;
    margin: 0;

    @mixin atSmall {
      font-size: 2.9rem;
    }
  }
  &__description {
    color: #FFF;
    font-size: 1.875rem;
    font-weight: 100;
    text-shadow: 2px 2px 0 rgba(0,0,0,.1);
    max-width: 30rem;
    margin-left: auto;
    margin-right: auto;
  }
```

### _mixins.css

```css
@define-mixin atSmall {
  @media (min-width: 530px) {
    @mixin-content;
  }
}
@define-mixin atMedium {
  @media (min-width: 800px) {
    @mixin-content;
  }
}
@define-mixin atLarge {
  @media (min-width: 1200px) {
    @mixin-content;
  }
}
```

----------

## 20: Responsive Images

**Responsive Image Situations:**

1. Art Direction and Cropping Situation - Picture Element

  ```html
  <picture>
    <source srcset="images/dog-crop-large.jpg" media="(min-width: 1200px)">"
    <source srcset="images/dog-crop-medium.jpg" media="(min-width: 760px)">
    <img src="images/dog-crop-small.jpg" alt="puppy in the sand">
  </picture>
  ```

2. Image Resolution and File Size Situation (Faster load times) A more hands-off approach

  ```html
  <img srcset="images/dog-resolution-small.jpg 570w, images/dog-resolution-medium.jpg 1200w, images/dog-resolution-large.jpg 1920w" alt="pupy in the sand">
  ```

----------

## 21: Tips for Testing Responsive Images

1. There are “-i” versions of all image files that have a dark overlay and the image size in white. Use these for testing.

2. Right-click “inspect” to open Developer Tools. Use this to test how the page will look on various screens.

---
