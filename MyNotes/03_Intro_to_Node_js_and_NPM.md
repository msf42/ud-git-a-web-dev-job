# 03: Intro to Node.js and NPM

## 06: The Need for Automation and Organization

Just notes on benefits of organizing code.

---

## 07: A Quick Note

No need to fully understand the code coming up. Focus on the GIT methods

---

## 08: Node.js Introduction

### What is Node.js?

**Javascript** -generally restricted to web browser, front end stuff. **Node.js** is a place for javascript to behave more like a full-fledged coding language; a playground where JS has access to the file system, databases, etc, and can behave more like a back-end language.

### 2 primary ways to use Node

- install it on a server - to serve up information so that you don’t need another language to do it

- install it on your personal computer - for development purposes

```js
  var fs = require('fs');
  var https = require('https');
  
  fs.writeFile(__dirname + "/index.html", "<h1>HTML is great!</h1>", function(error) {
  
    if (error) {
      return console.log(error);
    } else {
      return console.log("congrats!")
    }
  
  });
  
  var myPhotoLoc = 'https://raw.githubusercontent.com/LearnWebCode/welcome-to-git/master/images/dog.jpg'
  
  https.get(myPhotoLoc, function(response) {
  
    response.pipe(fs.createWriteStream(__dirname + "/dog.jpg"))
  
  })
```

---

## 09: NPM Introduction

### NPM = Node Package Manager

Used NPM to install packages into our folder.

```bash
  167  node -v # reveals version
  169  npm install jquery
  171  sudo rm node_modules/
  172  npm init
  173  npm install jquery --save
  177  npm install normalize.css --save
  178  git status
  179  git add package.json
  180  git status
  181  git commit -m 'Adding package.json file'
  182  git status
  183  git push
  184  git status
```

---

## 10: Important Note About Version Numbers

Downloaded new .json file to ensure my version numbers are those used in the course

---
