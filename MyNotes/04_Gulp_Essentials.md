# 04: Gulp Essentials

## 11: Gulp Introduction

A great link for Gulp:

[CSS Tricks - Gulp for Beginners](https://css-tricks.com/gulp-for-beginners/)

[Gulp](http://gulpjs.com/) is a tool that helps you out with several tasks when it comes to web development. It's often used to do front end tasks like:

- Spinning up a web server
- Reloading the browser automatically whenever a file is saved
- Using preprocessors like Sass or LESS
- Optimizing assets like CSS, JavaScript, and images

```js
  var gulp = require('gulp'),
  watch = require('gulp-watch'); // can use a comma and skip var

  // run with gulp defualt on command line
  gulp.task('default', function(){

    console.log("Hooray - you created a Gulp task");

  });

  gulp.task('html', function(){

    console.log("Imagine something useful being done to your html here");

  });

  gulp.task('styles', function(){

    console.log("Imagine SASS or PostCSS running here");

  });

  gulp.task('watch', function(){
    gulp.watch('index.html', ['html']); // watches index.html, then runs 'html' if changed
    gulp.watch('assets/styles/**/*.css', ['styles']);
  });
```

---

## 12: Gulp and Post CSS

### 3 Key Terms

- gulp.src() - the source for the file
- gulp.dest() - the destination
- pipe() - how the file is changed on the way

### CSS Before PostCSS

```css
  $mainBlue: #2f5572;

  body {
      color: $mainBlue;
      padding: 25px;
      margin: 10px;
      columns: 300px 2;
  }

  .box {
      a {
          display: block;
          padding: 10px;
      }
  }
```

### CSS After PostCSS

```css
  body {
      color: #2f5572;
      padding: 25px;
      margin: 10px;
      -webkit-columns: 300px 2;
         -moz-columns: 300px 2;
          columns: 300px 2;
  }

  .box a {
          display: block;
          padding: 10px;
      }
```

### gulpfile.js

```js
  var gulp = require('gulp'),
  watch = require('gulp-watch'),
  postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer'),
  cssvars = require('postcss-simple-vars'),
  nested = require('postcss-nested');

  gulp.task('default', function(){
    console.log("Hooray - you created a Gulp task");
  });

  gulp.task('html', function(){
    console.log("Imagine something useful being done to your html here");
  });

  gulp.task('styles', function(){
      return gulp.src('./app/assets/styles/style.css') // the source
       .pipe(postcss([cssvars, nested, autoprefixer])) // the changes to make
       .pipe(gulp.dest('./app/temp/styles')); // the destination
  });

  gulp.task('watch', function(){
    gulp.watch('./app/index.html', ['html']);
    gulp.watch('./app/assets/styles/**/*.css', ['styles']);
  });
```

---
