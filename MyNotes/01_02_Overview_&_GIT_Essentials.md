# 01/02: Overview & GIT Essentials

## 01: Course Overview

- Nice, detailed overview. Looks like it will be a good course!

---

## 02: What is GIT?

### What is GIT?

- A version control system
- History
- Collaboration
- Feature Branches

### Why should we learn GIT?

- A million reasons. Industry standard.

### Where do we begin?

Right fucking here.

---

## 03: See GIT in Action

### Some terms:

- Repository - folder where everything is saved for a project
- Working Directory - location on your computer that GIT is working from
- Commit - GIT doesn’t do anything until you make a commit. GIT’s way of saving
- Staging - Before we can commit changes, we must stage. Our way of having control of what is committed.
- Push = upload
- Pull = download

---

## 04: GIT Your Hands Dirty

```bash
  125  sudo apt-get install git
  126  git --version
  127  git config --global user.name "Steve"  # setting user name
  128  git config --global user.email "furches@gmail.com"  # setting email
  129  ls
  130  Project
  131  cd Project
  132  pwd
  133  mkdir "hello-world"
  134  ls
  135  cd hello-world/
  136  git init  # starts up GIT
  137  touch "index.html" # makes new file
  138  ls
  139  git status  # check status
  140  git add index.html  # add index.html to staging
  141  git status  # check status again
  142  git commit -m 'My first commit'  # make commit with comment
  143  git checkout -- .  # restores from git
  144  cd ..
  146  git clone https://github.com/LearnWebCode/welcome-to-git.git # cloning from github
```

---

## 05: Set Up Your Github

Already have an account, so made a new repository, and cloned from the course repository. Made a small change and pushed it to server.

```bash
  steve@steve-Inspiron-7537:~/Dropbox/_ACTIVE/Tech/GIT/Udemy_GIT_a_WebDevJob/Project$ git clone https://github.com/LearnWebCode/travel-site-files.git
  Cloning into 'travel-site-files'...
  remote: Enumerating objects: 83, done.
  remote: Total 83 (delta 0), reused 0 (delta 0), pack-reused 83
  Unpacking objects: 100% (83/83), done.

  steve@steve-Inspiron-7537:~/Dropbox/_ACTIVE/Tech/GIT/Udemy_GIT_a_WebDevJob/Project$ ls
  hello-world  UdemyGITCourse  welcome-to-git

  steve@steve-Inspiron-7537:~/Dropbox/_ACTIVE/Tech/GIT/Udemy_GIT_a_WebDevJob/Project$ cd UdemyGITCourse/

  steve@steve-Inspiron-7537:~/Dropbox/_ACTIVE/Tech/GIT/Udemy_GIT_a_WebDevJob/Project/UdemyGITCourse$ git remote set-url origin https://github.com/MSF42/UdemyGITCourse

  steve@steve-Inspiron-7537:~/Dropbox/_ACTIVE/Tech/GIT/Udemy_GIT_a_WebDevJob/Project/UdemyGITCourse$ git remote -v
  origin        https://github.com/MSF42/UdemyGITCourse (fetch)
  origin        https://github.com/MSF42/UdemyGITCourse (push)

  steve@steve-Inspiron-7537:~/Dropbox/_ACTIVE/Tech/GIT/Udemy_GIT_a_WebDevJob/Project/UdemyGITCourse$ git push origin master
  Username for 'https://github.com': MSF42
  Password for 'https://MSF42@github.com':
  Counting objects: 83, done.
  Delta compression using up to 4 threads.
  Compressing objects: 100% (81/81), done.
  Writing objects: 100% (83/83), 3.36 MiB | 351.00 KiB/s, done.
  Total 83 (delta 5), reused 0 (delta 0)
  remote: Resolving deltas: 100% (5/5), done.
  To https://github.com/MSF42/UdemyGITCourse
    * [new branch]      master -> master

  steve@steve-Inspiron-7537:~/Dropbox/_ACTIVE/Tech/GIT/Udemy_GIT_a_WebDevJob/Project/UdemyGITCourse$ git status
  On branch master
  Your branch is up to date with 'origin/master'.

  Changes not staged for commit:
    (use "git add <file>..." to update what will be committed)
    (use "git checkout -- <file>..." to discard changes in working directory)
  
          modified:   app/index.html
  
  no changes added to commit (use "git add" and/or "git commit -a")
  
  steve@steve-Inspiron-7537:~/Dropbox/_ACTIVE/Tech/GIT/Udemy_GIT_a_WebDevJob/Project/UdemyGITCourse$ git add -A
  
  steve@steve-Inspiron-7537:~/Dropbox/_ACTIVE/Tech/GIT/Udemy_GIT_a_WebDevJob/Project/UdemyGITCourse$ git status
  On branch master
  Your branch is up to date with 'origin/master'.
  
  Changes to be committed:
    (use "git reset HEAD <file>..." to unstage)
  
          modified:   app/index.html
  
  steve@steve-Inspiron-7537:~/Dropbox/_ACTIVE/Tech/GIT/Udemy_GIT_a_WebDevJob/Project/UdemyGITCourse$ git commit -m 'Added new meta keywords. GIT practice'
  [master feaac58] Added new meta keywords. GIT practice
    1 file changed, 1 insertion(+), 1 deletion(-)
  
  steve@steve-Inspiron-7537:~/Dropbox/_ACTIVE/Tech/GIT/Udemy_GIT_a_WebDevJob/Project/UdemyGITCourse$ git push origin master
  Username for 'https://github.com': MSF42
  Password for 'https://MSF42@github.com':
  Counting objects: 4, done.
  Delta compression using up to 4 threads.
  Compressing objects: 100% (4/4), done.
  Writing objects: 100% (4/4), 425 bytes | 425.00 KiB/s, done.
  Total 4 (delta 1), reused 0 (delta 0)
  remote: Resolving deltas: 100% (1/1), completed with 1 local object.
  To https://github.com/MSF42/UdemyGITCourse
```
