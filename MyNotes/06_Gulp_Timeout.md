# 06: Gulp Timeout

## 16: What is `browsersync?`

Very cool lesson on browsersync! It allows any changes in code to immediately be seen in the browser. You can also open the same url in multiple browsers; all changes - scrolling, forms, etc, will occur simultaneously.

---

## 17: Organizing Your `gulp.js` File

Much like we did previously with CSS, we split the gulp file up into smaller files.

### gulp.js

```js
  require('./gulp/tasks/styles'),
  require('./gulp/tasks/watch');
```

### styles.js

```js
  var gulp = require('gulp'),
  postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer'),
  cssvars = require('postcss-simple-vars'),
  nested = require('postcss-nested'),
  cssImport = require('postcss-import');
  gulp.task('styles', function(){
    return gulp.src('./app/assets/styles/style.css')
     .pipe(postcss([cssImport, cssvars, nested, autoprefixer]))
     .pipe(gulp.dest('./app/temp/styles'));
  });
```

### watch.js

```js
  var gulp = require('gulp'),
  watch = require('gulp-watch'),
  browserSync = require('browser-sync').create();
  gulp.task('watch', function(){
    browserSync.init({
      notify: false,
      server: {
        baseDir: "app"
      }
    });
    watch('./app/index.html', function() {
      browserSync.reload();
    });
    watch('./app/assets/styles/**/*.css', function() {
      gulp.start('cssInject');
    });
  });
  gulp.task('cssInject',['styles'] , function() {
    return gulp.src('./app/temp/styles/style.css')
      .pipe(browserSync.stream());
  });
```

---

## 18: Gulp Error Handling

### New style.js file

```js
  var gulp = require('gulp'),
  postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer'),
  cssvars = require('postcss-simple-vars'),
  nested = require('postcss-nested'),
  cssImport = require('postcss-import');
  gulp.task('styles', function(){
     return gulp.src('./app/assets/styles/style.css')
     .pipe(postcss([cssImport, cssvars, nested, autoprefixer]))
     .on('error', function(errorInfo) {
      console.log(errorInfo.toString());
      this.emit('end');
     })
    .pipe(gulp.dest('./app/temp/styles'));
  });
```

---
