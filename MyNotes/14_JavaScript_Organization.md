# 14: JavaScript Organization

## 39: Object-Oriented Programming

**Stop** thinking in terms of individual variables and functions
**Begin** thinking in terms of cohesive, self-sufficient objects.

An **object** has data and behavior (nouns and verbs).

When a function is part of an object, we call it a **method**. Greet is a method here.

```js
  var john = {
    name: "John Doe",
    favoriteColor: "blue",
    greet: function() {
    console.log("Hello. My name is " + john.name + " and my favorite color is " + john.favoriteColor + ".");
    }
  }
  john.greet()
```

### Constructors (like Classes in Python, but JS *technically* doesn’t have Classes)

`.this` is like `__self__` in Python

```js
  function Person(fullName, favColor) {
    this.name = fullName;
    this.favoriteColor = favColor;
    this.greet = function() {
    console.log("Hello. My name is " + this.name + " and my favorite color is " + this.favoriteColor + ".");
    }
  }

  var john = new Person("John Doe", "blue");
  john.greet();
  var jane = new Person("Jane Smith", "green");
  jane.greet();
```

---

## 40: The JS Module Pattern and “webpack”

[Webpack question on Udemy](https://www.udemy.com/git-a-web-developer-job-mastering-the-modern-workflow/learn/lecture/5432900#questions/5171062)

Lots of work figuring out webpack. Needed to do things a bit differently, but this solution really helped.

---

## 41: Integrating “webpack” into our Gulp Automation

All set up for automation. Added more to `gulp.watch` for the JS files

---

## 42: Tomorrow’s JavaScript Today (Babel)

Allows us to write modern JS, and write it in separate files. Babel will convert it all to a single file that can be read by any browser.

---
