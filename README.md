# Course Notes for Udemy GIT a Web Developer Job

My project and notes for Udemy - [GIT a Web Developer Job course](https://www.udemy.com/course/git-a-web-developer-job-mastering-the-modern-workflow/)

This is a guided code-along project I did as part of the course.

The course consisted of building a website using HTML/CSS/JS and using Github throughout the project. This was great practice using git, branching, merging, etc.

![certificate](GIT.jpg)

---
